#include "TCPDebugClass.h"

#define WAIT_PUSH 0
#define WAIT_BOUNCE 1
#define EMERGENCY_ACTION 2
#define SEND_FIREBASE 3
#define WAIT_NEXT 4

static int buttonState;
int buttonValue, closeFlag;
int emergencyButtonPin, emergencyLedPin;
unsigned long timeButton;


void EmergencyButton_init(int ebp, int elp){
  emergencyButtonPin = ebp;
  emergencyLedPin = elp;
  pinMode(emergencyButtonPin, INPUT);
  pinMode(emergencyLedPin, OUTPUT);
  digitalWrite(emergencyLedPin, LOW);
  Firebase_setEmergency(false);
  buttonState = 0;
  closeFlag = 0;
}


void EmergencyButton_motor(){
  switch(buttonState){
    case WAIT_PUSH:
      buttonValue = digitalRead(emergencyButtonPin);
      if (buttonValue == HIGH) {
        timeButton = millis();
        buttonState = WAIT_BOUNCE;
       }
       break;
    case WAIT_BOUNCE:
      if (timeButton + 30 < millis()){
        buttonValue = digitalRead(emergencyButtonPin);
        if (buttonValue == HIGH) {
          buttonState = EMERGENCY_ACTION;
        }
      }
      break;
    case EMERGENCY_ACTION:
      emergencyAction("\nEmergency button has been pressed!!");
      buttonState = SEND_FIREBASE;
      break;
    case SEND_FIREBASE:
      Firebase_setEmergency(true);
      timeButton = millis();
      buttonState = WAIT_NEXT;
    case WAIT_NEXT:
      if (timeButton + 1000 < millis()){
        buttonState = WAIT_PUSH;
      }
      break;
  }
}

void emergencyAction(char *message){
  closeFlag = 1;
  digitalWrite(emergencyLedPin, HIGH);
  Firebase_sendNotification();
  #ifdef  USE_SERIAL_DEBUG
  Serial.println(message);
  #endif
  //TCPDebug.println(message);
}

int getCloseFlag(){
  int cf = closeFlag;
  closeFlag = 0;
  return cf;
}