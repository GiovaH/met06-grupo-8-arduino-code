#include <FirebaseArduino.h>
#include <ESP8266HTTPClient.h>

#define FIREBASE_URL  "casovegaandroidgrupo8.firebaseio.com"
#define FIREBASE_SECRET "9V0TNoZJbnVrpTH6zXXSZVn6zCFRPcsOKv80wqCg"

#define WAIT_TAPS 0
#define SET_FLAG_UP 1
#define SET_FLAG_DOWN 2

static int fbState;
String chipID;

void Firebase_init(){
  Firebase.begin(FIREBASE_URL, FIREBASE_SECRET);
  fbState = 0;
  delay(25);
  chipID = ESP.getChipId();
  Firebase_setEmergency(false);
  Firebase_setPosition(0);
  Firebase_setFallDetected(false);
}

void Firebase_setTemperature(float tempC){
  Firebase.setFloat("/"+chipID+"/Temperature", tempC);
}

String Firebase_getToken(){
  String tok;
  tok = Firebase.getString("/"+chipID+"/Token");
  return tok;
}

float Firebase_getTemperature(){
  float tt = Firebase.getFloat("/"+chipID+"/Temperature");
  return tt;
}

void Firebase_setPosition(int pos){
  Firebase.setInt("/"+chipID+"/Position", pos);
}

void Firebase_setEmergency(boolean emer){
  Firebase.setBool("/"+chipID+"/EmergencyButton", emer);
}

void Firebase_setFallDetected(boolean fall){
  Firebase.setBool("/"+chipID+"/FallDetected", fall);
}


int Firebase_getTaps(){
  int taps = Firebase.getInt("/"+chipID+"/Taps");
  return taps;
}

void Firebase_setTaps(int value){
  Firebase.setInt("/"+chipID+"/Taps", value);
}

void Firebase_sendNotification(){
  HTTPClient http;
  String data = "{" ;
    data = data + "\"to\": \"" + Firebase_getToken() + "\"," ;
    data = data + "\"notification\": {" ;
    data = data + "\"body\": \"Algo no va bien... (caída/temperatura/botón de emergencia)\"," ;
    data = data + "\"title\" : \"Alerta\" " ;
    data = data + "} }" ;
    http.begin("http://fcm.googleapis.com/fcm/send");
    http.addHeader("Authorization", "key=AAAAHqHZmK4:APA91bGi1Ma-iypwMqAWWzj75xACwRkhw7Jok0-qh5aoUdGE1tEIWki9u1ZdqLaog3zCIfF6xvbqK9-KCvKYSy1Lui4D_pDSRt7HyypvuM6mSrmCr9Byk5TXTh4xDNWTgCNUDmJgyzER");
    http.addHeader("Content-Type", "application/json");
    http.addHeader("Host", "fcm.googleapis.com");
    http.addHeader("Content-Length", "58");
    http.POST(data);
    http.writeToStream(&Serial);
    http.end();
}

void Firebase_motor_taps(){
  switch(fbState){
    case WAIT_TAPS:
      if (Firebase_getTaps() == 1){
        Firebase_setTaps(0);
        fbState = SET_FLAG_UP;
      } else if (Firebase_getTaps() == 2){
        Firebase_setTaps(0);
        fbState = SET_FLAG_DOWN;
      } else {}
      break;
    case SET_FLAG_UP:
      UpDownButtons_setUpFlag(1);
      fbState = WAIT_TAPS;
      break;
    case SET_FLAG_DOWN:
      UpDownButtons_setDownFlag(1);
      fbState = WAIT_TAPS;
      break;
  }
}