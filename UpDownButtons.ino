#include "TCPDebugClass.h"

#define WAIT_PUSH 0
#define WAIT_BOUNCE 1
#define UP_ACTION 2
#define DOWN_ACTION 2
#define WAIT_NEXT 3

static int upButtonState, downButtonState;
int upButtonPin, downButtonPin;
int upButtonValue, downButtonValue, upFlag, downFlag;
unsigned long timeUpButton, timeDownButton;

void UpDownButtons_init(int ubp, int dbp){
  upButtonPin = ubp;
  downButtonPin = dbp;
  pinMode(upButtonPin, INPUT);
  pinMode(downButtonPin, INPUT);
  upButtonState = 0;
  downButtonState = 0;
  upFlag = 0;
  downFlag = 0;
}

void upButton_motor(){
  switch(upButtonState){
    case WAIT_PUSH:
      if (digitalRead(upButtonPin) == HIGH) {
        timeUpButton = millis();
        upButtonState = WAIT_BOUNCE;
       }
       break;
    case WAIT_BOUNCE:
      if (timeUpButton + 30 < millis()){
        upButtonValue = digitalRead(upButtonPin);
        if (upButtonValue == HIGH) {
          upButtonState = UP_ACTION;
        } else {
          upButtonState = WAIT_PUSH;
        }
      }
      break;
    case UP_ACTION:
      #ifdef  USE_SERIAL_DEBUG
      Serial.println("\nUP");
      #endif
      TCPDebug.println("\nUP");
      timeUpButton = millis();
      upButtonState = WAIT_NEXT;
      break;
    case WAIT_NEXT:
      upButtonValue = digitalRead(upButtonPin);
      if (upButtonValue == HIGH) {
        //nothin'
      } else if (upButtonValue == LOW) {
        upFlag = 1;
        upButtonState = WAIT_PUSH;
      }
      break;
    }
 }

void downButton_motor(){
  switch(downButtonState){
    case WAIT_PUSH:
      if (digitalRead(downButtonPin) == HIGH) {
        timeDownButton = millis();
        downButtonState = WAIT_BOUNCE;
      }
      break;
    case WAIT_BOUNCE:
      if (timeDownButton + 30 < millis()){
        if (digitalRead(downButtonPin) == HIGH) {
          downButtonState = DOWN_ACTION;
        } else {
          downButtonState = WAIT_PUSH;
        }
      }
      break;
    case DOWN_ACTION:
      #ifdef  USE_SERIAL_DEBUG
      Serial.println("\nDOWN");
      #endif
      TCPDebug.println("\nDOWN");
      timeDownButton = millis();
      downButtonState = WAIT_NEXT;
      break;
    case WAIT_NEXT:
      if (digitalRead(downButtonPin) == HIGH) {
        //nothin'
      } else {
        downFlag = 1;
        downButtonState = WAIT_PUSH;
      }
      break;
    }
 }

void UpDownButtons_setUpFlag(int m){
  upFlag = m;
}

void UpDownButtons_setDownFlag(int n){
  downFlag = n;
}

int getUpFlag(){
  int uf = upFlag;
  upFlag = 0;
  return (uf);
}

int getDownFlag(){
  int df = downFlag;
  downFlag = 0;
  return (df);
}