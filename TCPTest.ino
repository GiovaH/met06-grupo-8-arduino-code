#include <ESP8266WiFi.h>
#include <WiFiClient.h>


#include "TCPDebugClass.h"

/* AP configuration */
const char *ssid = "mySSID";
const char *password = "myPassword";

IPAddress serverIP(192,168,4,1);
IPAddress gateway(127,0,0,1);
IPAddress subnet(255,255,255,0);

long prevTime;
  
void TCPTest_setup() {
  
  WiFi.mode(WIFI_AP);
	
	/* Creating the access point */
	WiFi.softAP(ssid, password, 6);
  WiFi.softAPConfig(serverIP, gateway, subnet);

  /* Starting debugging server */
  TCPDebug.begin();
  
  /* Closing serial connection */
  //Serial.end();
  
  delay(100);
  
  prevTime = 0;
}

void TCPTest_loop() {

  if(!TCPDebug.hasClient())
    TCPDebug.checkForClient();

  if(prevTime + 20000 < millis()){
    prevTime = millis();
    TCPDebug.println("\nHan pasado otros veinte segundos");
  } 
}