#include <ESP8266WiFi.h>
#include <OneWire.h>
#include <DallasTemperature.h>
#include "TCPDebugClass.h"

#define ONE_WIRE_BUS 5
#define TAKE_TEMP 0
#define PRINT_TEMP 1
#define SEND_FIREBASE 2
#define WAIT_TEMP 3

OneWire oneWire(ONE_WIRE_BUS);
DallasTemperature DS18B20(&oneWire);
char tempCString[7];
unsigned long previousTime;
int tempState;
float tempC;
float aux;

void Temperature_init() {
  tempState = 0;
  DS18B20.begin();
}

void getTemperature() {
  do {
    DS18B20.requestTemperatures(); 
    tempC = DS18B20.getTempCByIndex(0);
    dtostrf(tempC, 2, 2, tempCString);
  } while (tempC == 85.0 || tempC == (-127.0));
}

void Temperature_motor(){
  switch(tempState){
    case TAKE_TEMP:
      getTemperature();
      if (tempC >= 32.50){
        emergencyAction("\nTemperature too high!");
      }
      tempState = 1;
      break;
    case PRINT_TEMP:
      #ifdef  USE_SERIAL_DEBUG
      Serial.print("\nTemperature: ");
      Serial.print(tempCString);
      Serial.print(" ºC");
      #endif
      TCPDebug.print("\nTemperature: ");
      TCPDebug.print(tempCString);
      TCPDebug.print(" ºC");
      tempState = 2;
      break;
    case SEND_FIREBASE:
      Firebase_setTemperature(tempC);
      tempState = WAIT_TEMP;
      break;
    case WAIT_TEMP:
      if (previousTime + 30000 < millis()){
        tempState = 0;
        previousTime = millis();
      }
      break;
  }
}